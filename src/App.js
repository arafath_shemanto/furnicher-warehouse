import { Route, Routes } from "react-router-dom";
import Blogs from "./components/Blogs/Blogs";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import Inventory from "./components/Inventory/Inventory";
import Login from "./components/Login/Login";
import Register from "./components/Register/Register";
import ManageItems from "./components/ManageItems/ManageItems";
import Myitems from "./components/Myitems/Myitems";
import Error_page from "./components/Error_page/Error_page";
import RequireAuth from "./components/Requireauth/RequireAuth";
import Add_inventory from "./components/Add_inventory/Add_inventory";

function App() {
  return (
    <div className="App">
      <Header></Header>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/blogs" element={<Blogs />}></Route>
        <Route path="/login" element={<Login />}></Route>
        <Route path="/register" element={<Register />}></Route>
        <Route
          path="/myitems"
          element={
            <RequireAuth>
              <Myitems />
            </RequireAuth>
          }
        ></Route>
        <Route
          path="/manageitems"
          element={
            <RequireAuth>
              <ManageItems />
            </RequireAuth>
          }
        ></Route>
        <Route
          path="/add_inventory"
          element={
            <RequireAuth>
              <Add_inventory />
            </RequireAuth>
          }
        ></Route>
        <Route
          path="/inventory/:id"
          element={
            <RequireAuth>
              <Inventory />
            </RequireAuth>
          }
        ></Route>
        <Route path="*" element={<Error_page />}></Route>
      </Routes>
      <Footer></Footer>
    </div>
  );
}

export default App;
