import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const Inventory = () => {
  const { id } = useParams();
  const navigation = useNavigate();
  const manageItemHandeler = () => {
    navigation("/manageitems");
  };
  const [product, setProduct] = useState({});
  useEffect(() => {
    const url = `https://intense-castle-89627.herokuapp.com/product/${id}`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setProduct(data));
  }, [id]);

  const { _id, img, name, prise, description, quantity, supplierName } =
    product;
  console.log(product);
  console.log(_id);
  // const deliverdHandeler = (id) => {
  //   const { quantity, ...rest } = product;
  //   if (quantity > 0) {
  //     const newCount = Number(quantity) - 1;
  //     console.log(quantity);
  //     const newProduct = { quantity: newCount, ...rest };
  //     setProduct(newProduct);
  //   }
  // };

  const addCountHandeler = (e) => {
    e.preventDefault();
    const inputVal = e.target.input.value;
    const newVal = parseInt(inputVal);
    const { quantity, ...rest } = product;
    const newQuentity = parseInt(quantity) + newVal;
    const updatedCounter = { quantity: newQuentity, ...rest };
    console.log(`https://intense-castle-89627.herokuapp.com/product/${_id}`);
    fetch(`https://intense-castle-89627.herokuapp.com/product/${_id}`, {
      method: "PUT", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(product),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setProduct(updatedCounter);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
    e.target.reset();
  };
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center mb-5">
              <h3>inventory {product._id}</h3>
            </div>
          </div>
          <div className="col-xl-6">
            <div className="product_widget11 mb_30 max_400px">
              <div className="product_thumb_upper">
                <a href="product_details.php" className="thumb">
                  <img src={product.img} alt="" />
                </a>
              </div>
              <div className="product__meta">
                <a href="product_details.php">
                  <h4>{product.name}</h4>
                </a>
                <p>{product.description?.slice(0, 76)}</p>
                <div className="product_prise ">
                  <p>prise: ${product.prise}</p>
                  <p>quantity available : {product.quantity} pices</p>
                  <p>sold : 122 pices</p>
                  <p>supplierName: {product.supplierName}</p>
                  <button
                    // onClick={() => deliverdHandeler(product._id)}
                    className="primary_btn3 small_btn radius_5px mt-2 w-100 text-center"
                  >
                    {product.quantity > 0 ? "Deliverd" : "Stock out"}
                  </button>
                </div>
              </div>
            </div>
            <button
              onClick={manageItemHandeler}
              className="primary_btn3   mt-2 w-100 text-center"
            >
              Manage Inventories
            </button>
          </div>
          <div className="col-xl-6">
            <h3 className="text-capitalize mb-4">update quantity here</h3>
            <form
            // onSubmit={addCountHandeler}
            >
              <div className="col-12 mb_20">
                <input
                  name="input"
                  placeholder="type quentity"
                  className="primary_input3 radius_5px"
                  type="text"
                  required
                />
                <button className="primary_btn3 small_btn radius_5px mt-2 w-100 text-center">
                  add quentity
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Inventory;
