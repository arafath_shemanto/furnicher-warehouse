import React, { useEffect, useState } from "react";
import icon1 from "../../assets/img/svg/google_icon.svg";
import loginImg from "../../assets/img/login_img.png";
import { Link, useNavigate } from "react-router-dom";
import auth from "../Firebase/Firebase.init";
import logo from "../../assets/img/logo.png";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  useCreateUserWithEmailAndPassword,
  useSignInWithGoogle,
} from "react-firebase-hooks/auth";
import { Spinner } from "react-bootstrap";

const Register = () => {
  const [signInWithGoogle, gogoleUser, gogoleLoading, gogoleError] =
    useSignInWithGoogle(auth);
  // email pass create user
  const [createUserWithEmailAndPassword, user, loading, error] =
    useCreateUserWithEmailAndPassword(auth, { sendEmailVerification: true });

  // set in state
  const [userInfo, setUser] = useState({
    email: "",
    password: "",
    confirmpassword: "",
  });
  const [customError, setCustomError] = useState({
    emailError: "",
    passwordError: "",
    confirmpasswordError: "",
    othersError: "",
  });

  // input handeler
  const emailHandler = (e) => {
    const regexEmail = /\S+@\S+\.\S+/;
    const validEmail = regexEmail.test(e.target.value);
    console.log(validEmail);
    if (validEmail) {
      setUser({ ...userInfo, email: e.target.value });
      setCustomError({ ...customError, emailError: "" });
    } else {
      setUser({ ...userInfo, email: "" });
      setCustomError({ ...customError, emailError: "type a valid email" });
    }
  };
  // pass handelr
  const passwordHandler = (e) => {
    const passwordRegex = /.{8,}/;
    const validPass = passwordRegex.test(e.target.value);
    if (validPass) {
      setUser({ ...userInfo, password: e.target.value });
      setCustomError({ ...customError, passwordError: "" });
    } else {
      setUser({ ...userInfo, password: "" });
      setCustomError({ ...customError, passwordError: "must be 8 charecter" });
    }
  };
  const confirmPasswordHandler = (e) => {
    if (e.target.value === userInfo.password) {
      setUser({ ...userInfo, confirmpassword: e.target.value });
      setCustomError({ ...customError, confirmpasswordError: "" });
    } else {
      setUser({ ...userInfo, confirmpassword: "" });
      setCustomError({
        ...customError,
        confirmpasswordError: "password doesnot matched",
      });
    }
  };
  //form handler
  const furnicher_formHandler = (e) => {
    e.preventDefault();
    createUserWithEmailAndPassword(userInfo.email, userInfo.password);
  };

  const navigate = useNavigate();

  useEffect(() => {
    if (user || gogoleUser) {
      toast(`login Successful`);
      navigate("/");
    }
  }, [user, gogoleUser]);
  useEffect(() => {
    if (error) {
      console.log(error);
      switch (error?.code) {
        case "auth/email-already-in-use":
          toast("This email already used");
          break;
        case "auth/invalid-email":
          toast("provide valid email");
          break;
        case "auth/invalid-email-verified":
          toast("invalid email verified");
          break;
        case "auth/invalid-password":
          toast("your password is invalid ");
          break;
        default:
          toast("Something Went Wrong !!!");
      }
    }
  }, [error]);
  if (error) {
    console.log(error);
  }
  if (loading || gogoleLoading) {
    return (
      <div className="loader_wrap">
        <Spinner animation="border" variant="success" />
      </div>
    );
  }
  return (
    <div className="amazy_login_area">
      <div className="amazy_login_area_left d-flex align-items-center justify-content-center">
        <div className="amazy_login_form">
          <Link to="/" className="logo mb_50 d-block">
            <img src={logo} alt="" />
          </Link>
          <h3 className="m-0">Sign Up</h3>
          <p className="support_text">
            See your growth and get consulting support!
          </p>
          <button
            onClick={() => signInWithGoogle()}
            className="google_logIn d-flex align-items-center justify-content-center w-100"
          >
            <img src={icon1} alt="" />
            <h5 className="m-0 font_16 f_w_500">Sign up with Google</h5>
          </button>
          <div className="form_sep2 d-flex align-items-center">
            <span className="sep_line flex-fill"></span>
            <span className="form_sep_text font_14 f_w_500 ">
              or Sign in with Email
            </span>
            <span className="sep_line flex-fill"></span>
          </div>
          <form onSubmit={furnicher_formHandler}>
            <div className="row">
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Email Address <span>*</span>{" "}
                </label>
                <input
                  name="name"
                  onChange={emailHandler}
                  placeholder="Enter  email"
                  className="primary_input3 radius_5px "
                  type="text"
                  required
                />
                {customError?.emailError && (
                  <p className="error_text">{customError?.emailError}</p>
                )}
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Password <span>*</span>
                </label>
                <input
                  name="name"
                  placeholder="Min. 8 Character"
                  onChange={passwordHandler}
                  className="primary_input3 radius_5px "
                  type="password"
                  required
                />
                {customError?.passwordError && (
                  <p className="error_text">{customError?.passwordError}</p>
                )}
              </div>
              <div className="col-12 mb_20">
                <label className="primary_label2">
                  Confrim Password <span>*</span>
                </label>
                <input
                  name="name"
                  onChange={confirmPasswordHandler}
                  placeholder="Min. 8 Character"
                  className="primary_input3 radius_5px "
                  type="password"
                  required
                />
                {customError?.confirmpasswordError && (
                  <p className="error_text">
                    {customError?.confirmpasswordError}
                  </p>
                )}
              </div>
              <div className="col-12">
                <button className="primary_btn3 style2 radius_5px  w-100 text-uppercase  text-center mb_25">
                  Sign Up
                </button>
              </div>
              <div className="col-12">
                <p className="sign_up_text">
                  Already have an Account?
                  <Link to="/login"> Sign in</Link>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div className="amazy_login_area_right d-flex align-items-center justify-content-center">
        <div className="amazy_login_area_right_inner d-flex align-items-center justify-content-center flex-column">
          <div className="thumb">
            <img className="img-fluid" src={loginImg} alt="" />
          </div>
          <div className="login_text d-flex align-items-center justify-content-center flex-column text-center">
            <h4>Turn your ideas into reality.</h4>
            <p className="m-0">
              Consistent quality and experience across all platforms and
              devices.
            </p>
          </div>
        </div>
      </div>
      <ToastContainer />
    </div>
  );
};

export default Register;
