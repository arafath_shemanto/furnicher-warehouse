import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Product from "../Product/Product";

const Products = () => {
  const navigation = useNavigate();
  const manageItemHandeler = () => {
    navigation("/manageitems");
  };
  const [service, setService] = useState([]);

  useEffect(() => {
    const url = `https://intense-castle-89627.herokuapp.com/products`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setService(data));
  }, []);
  return (
    <div className="home10_Newest_area section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-xl-12">
            <div className="row">
              <div className="col-12">
                <div className="theme_title text-center mb_55">
                  <h3>Newest Design</h3>
                  <p>
                    Find a bright ideal to suit your taste with our great
                    selection of suspension wall
                  </p>
                </div>
              </div>
            </div>
            <div className="row">
              {service.slice(0, 6).map((item) => (
                <Product item={item} key={item._id}></Product>
              ))}
            </div>
            <div className="row">
              <div className="col-12 text-center">
                <button
                  onClick={manageItemHandeler}
                  className="primary_btn3 radius_5px mt-4"
                >
                  Manage Inventories
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Products;
