import React from "react";

import img1 from "../../assets/img/brand/1.png";
import img2 from "../../assets/img/brand/2.png";
import img3 from "../../assets/img/brand/3.png";
import img4 from "../../assets/img/brand/4.png";
import img5 from "../../assets/img/brand/5.png";
import img6 from "../../assets/img/brand/6.png";
import img7 from "../../assets/img/brand/7.png";
import img8 from "../../assets/img/brand/8.png";
import img9 from "../../assets/img/brand/9.png";
import img10 from "../../assets/img/brand/10.png";

const Brand = () => {
  return (
    <div className="amaz_brand section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title d-flex align-items-center gap-3 mb_30">
              <h3 className="m-0 flex-fill">Top Brand</h3>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <div className="amazBrand_boxes">
              <a href="brand.php" className="single_brand d-block">
                <img src={img1} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img2} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img3} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img4} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img5} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img6} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img7} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img8} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img9} alt="" />
              </a>
              <a href="brand.php" className="single_brand d-block">
                <img src={img10} alt="" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Brand;
