import { signOut } from "firebase/auth";
import React from "react";
import { Container, Nav, Navbar, Spinner } from "react-bootstrap";
import { useAuthState } from "react-firebase-hooks/auth";
import { Link } from "react-router-dom";
import logo from "../../assets/img/logo.png";
import auth from "../Firebase/Firebase.init";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Loaders from "../Loaders/Loaders";
const Header = () => {
  const [user, loading, error] = useAuthState(auth);
  const logout = () => {
    toast("logout successfully");
    signOut(auth);
  };
  if (loading) {
    return (
      <div className="loader_wrap">
        <Spinner animation="border" variant="success" />
      </div>
    );
  }
  return (
    <Navbar
      collapseOnSelect
      className="header_area"
      expand="lg"
      bg="light"
      variant="light"
    >
      <Container fluid>
        <Link to="/">
          <img src={logo} alt="" />
        </Link>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse className="main_menu" id="responsive-navbar-nav ">
          <Nav className="me-auto ">
            <Link className="menu_item" to="/">
              home
            </Link>
            <Link className="menu_item" to="/blogs">
              blogs
            </Link>
          </Nav>
          <Nav className="align-items-center">
            <Link className="menu_item" to="/manageitems">
              manage inventory
            </Link>
            <Link className="menu_item" to="/myitems">
              myitems
            </Link>
            <Link className="menu_item" to="/add_inventory">
              add inventory
            </Link>
            {user?.email ? (
              <div>
                <button
                  className="primary_btn3 small_btn radius_5px"
                  onClick={logout}
                >
                  logout
                </button>
              </div>
            ) : (
              <Link to="/login" className="primary_btn3 small_btn radius_5px">
                login
              </Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
      <ToastContainer />
    </Navbar>
  );
};

export default Header;
