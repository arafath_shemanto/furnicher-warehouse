// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBK_Hb4BjtYbj-nlYy7YS9RtMGcc3rPcPU",
  authDomain: "furniture-warehouse-fa81e.firebaseapp.com",
  projectId: "furniture-warehouse-fa81e",
  storageBucket: "furniture-warehouse-fa81e.appspot.com",
  messagingSenderId: "65076725108",
  appId: "1:65076725108:web:fd15134bfef2ad3d029821",
  measurementId: "G-GLYMFEVNXN",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
export default auth;
