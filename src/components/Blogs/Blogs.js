import React from "react";

const Blogs = () => {
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title text-center">
              <h3>blogs</h3>
            </div>
          </div>
          <div className="col-12">
            <h3>Q.1 Difference between javascript and nodejs</h3>
            <p>
              <strong>javascript Ans: </strong> javascript is programming
              language and it can run only bouser . it can run any kind of
              bouser. React Vue Angular are its most popular library and
              freamwork . it used to develop frontend
            </p>
            <p>
              <strong>nodejs Ans: </strong> nodejs is javascript runtime. it
              used for develop backend . expressJs is nodejs micro freamwork .
              node js use V8 engine
            </p>
            <hr />
          </div>
          <div className="col-12">
            <h3>
              Q.2 When should you use nodejs and when should you use mongodb
            </h3>
            <p>
              <strong>Ans: </strong> nodejs is javascript runtime. it made to
              develop website backend. it use V8 engine. when you need to
              develop website then you need to use node js . MongoDB is data
              base . MongoDB powers faster, more flexible application. when you
              want to store data on a server then you can use this
            </p>
            <hr />
          </div>
          <div className="col-12">
            <h3>Q.3 Differences between sql and nosql databases.</h3>
            <p>
              <strong>Ans: </strong> SQL is Structured Query Language. SQL is
              Vertically Scalable. its best for complex queries. its not so good
              for hierarchical data storage. noSQL is Non-relational or
              distributed database system. its not good for complex queries and
              its good for hierarchical data storage.
            </p>
            <hr />
          </div>
          <div className="col-12">
            <h3>Q.4 What is the purpose of jwt and how does it work</h3>
            <p>
              <strong>Ans: </strong> the purpose of jwt is secure information
              between a client and a server. without it anyone can use data by
              path url. jwt help to secure this information. its differ from
              other web tokens in that they contain a set of claims .this Claims
              are used to transmit information between client and a server. its
              mainly works by tokens pass
            </p>
            <hr />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Blogs;
