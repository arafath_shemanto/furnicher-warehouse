import React, { useEffect, useState } from "react";

const UseInventoryHook = (id) => {
  const [product, setProduct] = useState({});
  useEffect(() => {
    const url = `https://intense-castle-89627.herokuapp.com/product/${id}`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setProduct(data));
  }, [id]);
  return [product, setProduct];
};

export default UseInventoryHook;
