import React, { useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../Firebase/Firebase.init";

const Add_inventory = () => {
  // const { _id, img, name, prise, description, quantity, supplierName }
  const [user, loading, error] = useAuthState(auth);
  const submitHandeler = (e) => {
    e.preventDefault();
    const email = e.target.email.value;
    const image = e.target.image.value;
    const prise = e.target.prise.value;
    const item_Name = e.target.item_Name.value;
    const description = e.target.description.value;
    const quantity = e.target.quantity.value;
    const supplier = e.target.supplier.value;
    console.log(image, quantity, supplier, description, item_Name);
    const product = {
      email,
      image,
      prise,
      item_Name,
      description,
      quantity,
      supplier,
    };
    fetch("https://intense-castle-89627.herokuapp.com/newitem", {
      method: "POST", // or 'PUT'
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(product),
    })
      .then((response) => response.json())
      .then((data) => {
        console.log("Success:", product);
      })
      .catch((error) => {
        console.error("Error:", error);
      });
    e.target.reset();
    console.log(product);
  };

  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-12">
            <div className="theme_title mb_40 text-center">
              <h3>Add your item</h3>
            </div>
          </div>
          <div className="col-xl-6">
            <form onSubmit={submitHandeler}>
              <div className="row">
                <div className="col-12">
                  <label className="primary_label2">
                    Email <span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    className="primary_input3 radius_5px mb_20"
                    name="email"
                    value={user?.email}
                    readOnly
                    disabled
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Img Url<span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    name="image"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Item Name <span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    name="item_Name"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Prise <span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    name="prise"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Description <span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    name="description"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Quantity <span>*</span>
                  </label>
                  <input
                    type="number"
                    placeholder=""
                    name="quantity"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <label className="primary_label2">
                    Supplier Name <span>*</span>
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    name="supplier"
                    className="primary_input3 radius_5px mb_20"
                    required
                  />
                </div>
                <div className="col-12">
                  <button className="primary_btn3  radius_5px w-100 text-center">
                    add item
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Add_inventory;
