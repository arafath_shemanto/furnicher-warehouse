import React from "react";
import { Link } from "react-router-dom";
import error_img from "../../assets/img/error_img.png";
const Error_page = () => {
  return (
    <div className="error_area">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="error_inner">
              <div className="thumb">
                <img className="img-fluid" src={error_img} alt="" />
              </div>
              <h3>Opps! Page Not Found</h3>
              <p>
                Perhaps you can try to refresh the page, sometimes it works.
              </p>
              <Link
                to="/"
                className="primary_btn3 min_200 style6 f_w_700 radius_3px"
              >
                Back To Homepage
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Error_page;
