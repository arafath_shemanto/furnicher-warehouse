import React from "react";

const Feature = () => {
  return (
    <div className="home10_feature_area">
      <div className="single_feature home10_featureBg_1">
        <h3>British Made Pocket Knife - Oak</h3>
        <a href="product.php">Discover Now</a>
      </div>
      <div className="single_feature home10_featureBg_2">
        <h3>Juicy Pendant Chair Creative Design</h3>
        <a href="product.php">Discover Now</a>
      </div>
      <div className="single_feature home10_featureBg_3">
        <h3>British Made Pocket Knife - Oak</h3>
        <a href="product.php">Discover Now</a>
      </div>
    </div>
  );
};

export default Feature;
