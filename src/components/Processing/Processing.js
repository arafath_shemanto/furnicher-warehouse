import React from "react";

import img1 from "../../assets/img/svg/delevary_10.svg";
import img2 from "../../assets/img/svg/clock_10.svg";
import img3 from "../../assets/img/svg/help_10.svg";
import img4 from "../../assets/img/svg/payment_10.svg";

const Processing = () => {
  return (
    <div className="home10_processing_wrapper">
      <div className="container">
        <div className="processing_box">
          <div className="row">
            <div className="col-xl-3 col-md-6">
              <div className="single_processing">
                <div className="icon">
                  <img src={img1} alt="" />
                </div>
                <div className="processign_content">
                  <h4>Free Delivery</h4>
                  <p>Free Shipping Order $500+</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="single_processing">
                <div className="icon">
                  <img src={img2} alt="" />
                </div>
                <div className="processign_content">
                  <h4>90 Days Return</h4>
                  <p>Change of mind is’t applicable</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="single_processing">
                <div className="icon">
                  <img src={img4} alt="" />
                </div>
                <div className="processign_content">
                  <h4>Secure Payment</h4>
                  <p>Secure Payment Methods</p>
                </div>
              </div>
            </div>
            <div className="col-xl-3 col-md-6">
              <div className="single_processing">
                <div className="icon">
                  <img src={img3} alt="" />
                </div>
                <div className="processign_content">
                  <h4>24/7 Help Center</h4>
                  <p>Round-the-clock Helpline</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Processing;
