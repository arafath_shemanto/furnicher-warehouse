import React from "react";
import { Link, useNavigate } from "react-router-dom";
const Product = ({ item }) => {
  const { _id, name, img, description, prise, quantity, supplierName } = item;
  const navigate = useNavigate();
  const productHandeler = (id) => {
    navigate(`/inventory/${id}`);
  };
  return (
    <div className="col-xl-3 col-lg-3 col-md-6">
      <div className="product_widget11 mb_30">
        <div className="product_thumb_upper">
          <a href="#" className="thumb">
            <img src={img} alt="" />
          </a>
        </div>
        <div className="product__meta">
          <a href="product_details.php">
            <h4>{name}</h4>
          </a>
          <p>{description?.slice(0, 76)}</p>
          <div className="product_prise ">
            <p>prise: ${prise}</p>
            <p>quantity available : {quantity} pices</p>
            <p>supplierName: {supplierName}</p>
            <button
              onClick={() => productHandeler(_id)}
              className="primary_btn3 small_btn radius_5px mt-2"
            >
              Update
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Product;
