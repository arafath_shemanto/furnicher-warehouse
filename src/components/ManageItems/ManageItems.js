import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const ManageItems = () => {
  const navigation = useNavigate();
  const addNewItem = () => {
    navigation("/add_inventory");
  };
  const [service, setService] = useState([]);

  useEffect(() => {
    const url = `https://intense-castle-89627.herokuapp.com/products`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setService(data));
  }, []);
  const deleteHandeler = (id) => {
    const url = `https://intense-castle-89627.herokuapp.com/product/${id}`;
    fetch(url, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const confirm = window.confirm("want to delete this item?");
        if (confirm) {
          const dataHave = service.filter((item) => item._id !== id);
          setService(dataHave);
        }
      });
  };
  console.log(service);
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="col-12  d-flex align-items-center gap-2 mb-3">
              <h3 className="fs-5 flex-fill">Manage product</h3>
              <button
                onClick={addNewItem}
                className="primary_btn3 small_btn radius_5px mt-2"
              >
                add new item
              </button>
            </div>
          </div>
        </div>
        <div className="row">
          {service.map((item) => (
            <div key={item._id} className="col-xl-3 col-md-6">
              <div className="product_widget11 mb_30">
                <div className="product_thumb_upper">
                  <a href="#" className="thumb">
                    <img src={item.img} alt="" />
                  </a>
                </div>
                <div className="product__meta">
                  <a href="#">
                    <h4>{item.name}</h4>
                  </a>
                  <p>{item.description?.slice(0, 76)}</p>
                  <div className="product_prise ">
                    <p>prise: ${item.prise}</p>
                    <button
                      onClick={() => deleteHandeler(item._id)}
                      className="primary_btn3 small_btn radius_5px mt-2"
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ManageItems;
