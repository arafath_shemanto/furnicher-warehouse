import React from "react";

import payments from "../../assets/img/payments.png";
const Footer = () => {
  return (
    <footer className="home_ten_footer">
      <div className="main_footer_wrap">
        <div className="container">
          <div className="row">
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>PRODUCTS</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="product.php">For Men</a>
                  </li>
                  <li>
                    <a href="product.php">For Woman</a>
                  </li>
                  <li>
                    <a href="product.php">Accessories</a>
                  </li>
                  <li>
                    <a href="product.php">Collections</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>SERVICES</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="product.php">Apps</a>
                  </li>
                  <li>
                    <a href="product.php">Services</a>
                  </li>
                  <li>
                    <a href="product.php">Photo Books</a>
                  </li>
                  <li>
                    <a href="product.php">Lifecake</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>SUPPORT</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="product.php">Downloads</a>
                  </li>
                  <li>
                    <a href="faq.php">FAQs</a>
                  </li>
                  <li>
                    <a href="contact.php">Repair Centres</a>
                  </li>
                  <li>
                    <a href="contact.php">Contact Support</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-2 col-lg-3 col-md-6">
              <div className="footer_widget">
                <div className="footer_title">
                  <h3>GET INSPIRED</h3>
                </div>
                <ul className="footer_links">
                  <li>
                    <a href="product.php">Inspiration</a>
                  </li>
                  <li>
                    <a href="faq.php">Tips & Tutorials</a>
                  </li>
                  <li>
                    <a href="product.php">YouConnect</a>
                  </li>
                  <li>
                    <a href="lookbook.php">Gallery</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-xl-4 col-lg-3 col-md-6">
              <div className="footer_widget">
                <p className="large_text">
                  All-in-one WooCommerce WordPress Theme is a specific, clean,
                  minimal and ultimate theme.
                </p>
                <p className="address_text">
                  {" "}
                  <span>Address:</span>947 Clare Point, Western Sahara
                </p>
                <p className="address_text">
                  {" "}
                  <span>Support:</span>(01) 00 230 6789
                </p>
                <p className="address_text">
                  {" "}
                  <span>Email: </span> info@example.com
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="copyright_area">
        <div className="container">
          <div className="row align-items-center">
            <div className="col-md-12">
              <div className="copy_right_text">
                <p>
                  © 2022 <a href="#">Furniture Warehouse.</a> All rights
                  reserved. Made By <a href="#">Arafath Hossain</a>.
                </p>
                <div className="payment_imgs ">
                  <img src={payments} alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
