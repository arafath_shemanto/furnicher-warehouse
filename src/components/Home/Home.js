import React from "react";
import Banner from "../Banner/Banner";
import Brand from "../Brand/Brand";
import Feature from "../Feature/Feature";
import Processing from "../Processing/Processing";
import Products from "../Products/Products";

const Home = () => {
  return (
    <div>
      <Banner></Banner>
      <Feature></Feature>
      <Products></Products>
      <Processing></Processing>
      <Brand></Brand>
    </div>
  );
};

export default Home;
