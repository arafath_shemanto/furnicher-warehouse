import React, { useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import auth from "../Firebase/Firebase.init";

const Myitems = () => {
  const [products, setProducts] = useState([]);
  const [user] = useAuthState(auth);
  useEffect(() => {
    const email = user?.email;
    console.log(email);
    const url = `https://intense-castle-89627.herokuapp.com/newitem?email=${email}`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => setProducts(data));
  }, [user]);
  console.log(products);

  const deleteHandeler = (id) => {
    const url = `https://intense-castle-89627.herokuapp.com/newitem/${id}`;

    fetch(url, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        const confirm = window.confirm("want to delete this item?");
        if (confirm) {
          const dataHave = products.filter((item) => item._id !== id);
          setProducts(dataHave);
        }
      });
  };
  return (
    <div className="section_spacing">
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="theme_title">
              <h3> my items {products.length}</h3>
            </div>
          </div>
          {products.map((item) => (
            <div className="col-xl-3 col-lg-3 col-md-6">
              <div className="product_widget11 mb_30">
                <div className="product_thumb_upper">
                  <a href="#" className="thumb">
                    <img src={item?.image} alt="" />
                  </a>
                </div>
                <div className="product__meta">
                  <a href="product_details.php">
                    <h4>{item?.item_Name}</h4>
                  </a>
                  <p>{item?.description}</p>
                  <div className="product_prise ">
                    <p>prise: ${item?.prise}</p>
                    <button
                      onClick={() => deleteHandeler(item?._id)}
                      className="primary_btn3 small_btn radius_5px mt-2"
                    >
                      delete
                    </button>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Myitems;
