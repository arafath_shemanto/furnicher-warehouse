import React from "react";
import { Link } from "react-router-dom";

const Banner = () => {
  return (
    <div className="banner_ten mb_30">
      <div className="banner_single">
        <div className="container-fluid">
          <div className="row ">
            <div className="col-xl-12 d-flex justify-content-end">
              <div className="banner__text">
                <span>sophisticated build</span>
                <h3>Smooth Finish Aluminum</h3>
                <p>
                  Pleasant, glare-free illumination delivered via sculptural
                  fixtures.
                </p>
                <Link to="/manageitems" className="primary_btn">
                  Manage All Product
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Banner;
